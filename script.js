document.querySelector("#btn").addEventListener("click", (e) => {
    e.preventDefault();
    async function renderInfo() {
      const userIp = await fetch("https://api.ipify.org/?format=json")
      .then((response) => response.json());
      const location = await fetch(`http://ip-api.com/json/${userIp.ip}`)
      .then((response) => response.json());
      showAddress(location);
    }
    renderInfo();
  });
  function showAddress( {timezone, country, regionName, city, region} ) {
    const userInfo = document.createElement("div");
    const container = document.querySelector(".info")
    userInfo.innerHTML = `<p>Continent: ${timezone}</p><p>Country: ${country}</p><p>Region: ${regionName}</p><p>City: ${city}</p><p>District: ${region}</p>`;
    container.append(userInfo);
  }
  
  
  
  